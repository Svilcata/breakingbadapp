package com.example.breakingbadapp.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities


object Utils {
    fun isNetworkOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capability =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        return capability?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) ?: false
    }
}