package com.example.breakingbadapp.extensions

import android.content.DialogInterface
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment

fun Fragment.showErrorDialog(
    @NonNull @StringRes title: Int,
    @NonNull message: String,
    @Nullable @StringRes buttonText: Int?,
    @Nullable listener: DialogInterface.OnClickListener?
) {
    if (isAdded) {
        val builder = AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(message)
        if (buttonText != null) {
            builder.setPositiveButton(buttonText, listener)
        } else {
            builder.setPositiveButton(android.R.string.ok, listener)
        }
        builder.show()
    }
}