package com.example.breakingbadapp.api

import com.example.breakingbadapp.api.breakingbad.ApiDefinitionBreakingBad
import com.example.breakingbadapp.api.response.CharacterResponse
import com.example.breakingbadapp.api.retrofit.ApiHelper

class BreakingBadRepo {

    private var client: ApiDefinitionBreakingBad = ApiHelper.apiDefinition

    suspend fun getCharacters(): List<CharacterResponse> = client.getCharacters()
}