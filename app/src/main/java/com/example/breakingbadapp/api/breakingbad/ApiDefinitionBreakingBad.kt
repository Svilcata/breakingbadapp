package com.example.breakingbadapp.api.breakingbad

import com.example.breakingbadapp.api.response.CharacterResponse
import retrofit2.http.GET

/**
 *  Definition for breaking bad ApiDefinition methods
 */
interface ApiDefinitionBreakingBad {

    @GET("/api/characters")
    suspend fun getCharacters(): List<CharacterResponse>
}