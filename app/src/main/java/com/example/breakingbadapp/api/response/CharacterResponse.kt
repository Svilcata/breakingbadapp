package com.example.breakingbadapp.api.response

import com.google.gson.annotations.SerializedName

data class CharacterResponse(
    @SerializedName("appearance")
    val appearance: List<Int>,
    @SerializedName("better_call_saul_appearance")
    val better_call_saul_appearance: List<Int>,
    @SerializedName("birthday")
    val birthday: String,
    @SerializedName("category")
    val category: String,
    @SerializedName("char_id")
    val char_id: Int,
    @SerializedName("img")
    val img: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("nickname")
    val nickname: String,
    @SerializedName("occupation")
    val occupation: List<String>,
    @SerializedName("portrayed")
    val portrayed: String,
    @SerializedName("status")
    val status: String
)