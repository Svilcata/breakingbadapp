package com.example.breakingbadapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableList
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.example.breakingbadapp.BR

abstract class BaseRecyclerAdapter<T>(var items: ObservableList<T>) :
    RecyclerView.Adapter<BaseRecyclerAdapter<T>.BaseViewHolder<T, ViewDataBinding>>() {

    @get:LayoutRes
    protected abstract var layoutId: Int
    protected abstract var viewModel: ViewModel
    var lifecycleOwner: LifecycleOwner? = null

    private var onListChangedCallback =
        object : ObservableList.OnListChangedCallback<ObservableList<T>>() {
            override fun onChanged(sender: ObservableList<T>) {
                notifyDataSetChanged()
            }

            override fun onItemRangeRemoved(
                sender: ObservableList<T>,
                positionStart: Int,
                itemCount: Int
            ) {
                notifyItemRangeRemoved(positionStart, itemCount)
            }

            override fun onItemRangeMoved(
                sender: ObservableList<T>,
                fromPosition: Int,
                toPosition: Int,
                itemCount: Int
            ) {
                notifyDataSetChanged()
            }

            override fun onItemRangeInserted(
                sender: ObservableList<T>,
                positionStart: Int,
                itemCount: Int
            ) {
                notifyItemRangeInserted(positionStart, itemCount)
            }

            override fun onItemRangeChanged(
                sender: ObservableList<T>,
                positionStart: Int,
                itemCount: Int
            ) {
                notifyItemRangeChanged(positionStart, itemCount)
            }
        }

    init {
        items.addOnListChangedCallback(onListChangedCallback)
    }

    private fun getViewHolderBinding(parent: ViewGroup, @LayoutRes itemLayoutId: Int): ViewDataBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            itemLayoutId,
            parent,
            false
        )

    override fun onBindViewHolder(holder: BaseViewHolder<T, ViewDataBinding>, position: Int) {
        holder.bind(items[position], holder.binder)
        holder.binder?.executePendingBindings()
    }

    override fun getItemCount(): Int {
        if (items != null) {
            return items.size
        }
        return 0
    }

    fun setItems(items: ObservableList<T>, vm: ViewModel, itemLayoutId: Int?) {
        this.items = items
        this.viewModel = vm
        if (itemLayoutId != null) {
            this.layoutId = itemLayoutId
        }
        items.addOnListChangedCallback(onListChangedCallback)
        notifyDataSetChanged()
    }

    @NonNull
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<T, ViewDataBinding> = object :
        BaseViewHolder<T, ViewDataBinding>((getViewHolderBinding(parent, layoutId)).root) {}


    abstract inner class BaseViewHolder<T, B : ViewDataBinding> constructor(view: View) :
        RecyclerView.ViewHolder(view) {

        val binder: B? = DataBindingUtil.bind(view)

        fun bind(item: T, binder: ViewDataBinding?) {
            binder?.setVariable(BR.vm, viewModel)
            binder?.setVariable(BR.item, item)
            if (lifecycleOwner != null) {
                binder?.lifecycleOwner = lifecycleOwner
            }
        }
    }
}