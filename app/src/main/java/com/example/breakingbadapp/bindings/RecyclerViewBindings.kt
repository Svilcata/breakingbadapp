package com.example.breakingbadapp.bindings

import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableList
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.breakingbadapp.adapter.BaseRecyclerAdapter

@BindingAdapter("viewModel", "items", "itemLayoutId", "orientation", "lifecycleOwner", "divider", requireAll = false)
fun <T> bindViewModel(view: RecyclerView,
                      vm: ViewModel,
                      items: ObservableList<T>,
                      itemLayoutId: Int,
                      orientation: Int?,
                      lifecycleOwner: LifecycleOwner?,
                      divider: Boolean
) {

    if (view.layoutManager == null) {
        view.layoutManager = LinearLayoutManager(
                view.context, orientation
                ?: RecyclerView.VERTICAL, false
        )
    }

    if (divider){
        view.addItemDecoration(DividerItemDecoration(view.context, orientation
            ?: RecyclerView.VERTICAL))
    }

    if (view.adapter == null) {
        view.adapter = object : BaseRecyclerAdapter<T>(items) {
            override var layoutId: Int = itemLayoutId
            override var viewModel: ViewModel = vm
        }
    } else {
        (view.adapter as BaseRecyclerAdapter<T>).setItems(items, vm, itemLayoutId)
    }

    (view.adapter as BaseRecyclerAdapter<T>).lifecycleOwner = lifecycleOwner
}
