package com.example.breakingbadapp.bindings

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.breakingbadapp.R

@BindingAdapter("path", requireAll = false)
fun setPath(view: ImageView, path: String?) {
    Glide.with(view.context).load(path).placeholder(R.drawable.missing_photo).into(view)
}