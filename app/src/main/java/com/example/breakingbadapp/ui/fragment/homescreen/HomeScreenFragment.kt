package com.example.breakingbadapp.ui.fragment.homescreen

import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.lifecycle.Observer
import com.example.breakingbadapp.R
import com.example.breakingbadapp.databinding.FragmentHomeScreenBinding
import com.example.breakingbadapp.extensions.showErrorDialog
import com.example.breakingbadapp.ui.activity.main.MainActivity
import com.example.breakingbadapp.ui.event.EventNavigate
import com.example.breakingbadapp.ui.event.EventShowSeasonDropdown
import com.example.breakingbadapp.ui.fragment.BaseFragment
import com.example.breakingbadapp.ui.fragment.details.DetailsFragment
import com.example.breakingbadapp.utils.Utils

class HomeScreenFragment : BaseFragment<FragmentHomeScreenBinding>() {

    companion object {
        fun newInstance() = HomeScreenFragment()
    }

    override val layoutId: Int = R.layout.fragment_home_screen

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkPermissions()

        subscribe(EventNavigate::class, Observer {
            activity?.run {
                (activity as MainActivity).replaceFragment(DetailsFragment.newInstance())
            }
        })

        subscribe(EventShowSeasonDropdown::class, Observer {
            val popupMenu = PopupMenu(requireContext(), binding.imgBtnFilter)
            popupMenu.menuInflater.inflate(R.menu.menu_seasons, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener {
                viewModel.onSeasonSelected(it.title.toString())
                true
            }
            popupMenu.show()
        })
    }

    private fun checkPermissions() {
        if (!Utils.isNetworkOnline(requireContext())) {
            showErrorDialog(
                R.string.error,
                getString(R.string.error_internet_connection_lost),
                null,
                null
            )
        } else {
            viewModel.loadCharacterList()
        }
    }
}
