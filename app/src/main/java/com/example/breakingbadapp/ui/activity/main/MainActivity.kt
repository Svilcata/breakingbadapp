package com.example.breakingbadapp.ui.activity.main

import android.os.Bundle
import com.example.breakingbadapp.ui.activity.BaseActivity
import com.example.breakingbadapp.ui.fragment.homescreen.HomeScreenFragment

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addFragment(HomeScreenFragment.newInstance())
    }
}
