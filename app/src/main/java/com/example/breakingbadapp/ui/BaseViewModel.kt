package com.example.breakingbadapp.ui

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.example.breakingbadapp.ui.event.base.LiveEvent
import com.example.breakingbadapp.ui.event.base.LiveEventMap
import kotlin.reflect.KClass

open class BaseViewModel : ViewModel() {

    private val liveEventMap = LiveEventMap()

    fun <T : LiveEvent> subscribe(
        lifecycleOwner: LifecycleOwner,
        eventClass: KClass<T>,
        eventObserver: Observer<T>
    ) {
        liveEventMap.subscribe(lifecycleOwner, eventClass, eventObserver)
    }

    protected fun <T : LiveEvent> publish(event: T) {
        liveEventMap.publish(event)
    }
}