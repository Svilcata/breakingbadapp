package com.example.breakingbadapp.ui.fragment

import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.breakingbadapp.api.BreakingBadRepo
import com.example.breakingbadapp.api.response.CharacterResponse
import com.example.breakingbadapp.ui.BaseViewModel
import com.example.breakingbadapp.ui.event.EventNavigate
import com.example.breakingbadapp.ui.event.EventShowSeasonDropdown
import kotlinx.coroutines.launch

class SharedViewModel : BaseViewModel(), LifecycleObserver {

    private val repository = BreakingBadRepo()

    var isListLoaded = MutableLiveData<Boolean>().apply { value = false }

    private val characterList = ArrayList<CharacterResponse>()
    var filteredCharacterList = ObservableArrayList<CharacterResponse>()

    var txtSearchCharacter = MutableLiveData<String>()

    val characterImg = MutableLiveData<String>()
    val characterName = MutableLiveData<String>()
    val characterOccupation = ObservableArrayList<String>()
    val characterStatus = MutableLiveData<String>()
    val characterNickname = MutableLiveData<String>()
    val characterSeasonAppearance = ObservableArrayList<Int>()

    fun onCharacterClicked(character: CharacterResponse) {
        characterImg.value = character.img
        characterName.value = character.name
        characterStatus.value = character.status
        characterNickname.value = character.nickname

        if (characterOccupation.isNotEmpty()) {
            characterOccupation.clear()
        }

        if (characterSeasonAppearance.isNotEmpty()) {
            characterSeasonAppearance.clear()
        }

        characterOccupation.addAll(character.occupation)
        characterSeasonAppearance.addAll(character.appearance)

        publish(EventNavigate())
    }

    init {
        txtSearchCharacter.observeForever { text ->
            if (text.isEmpty()) {
                filteredCharacterList.clear()
                filteredCharacterList.addAll(characterList)
            }
        }
    }

    fun onSearchClicked() {
        if (!txtSearchCharacter.value.isNullOrEmpty()) {
            filteredCharacterList.clear()
            filteredCharacterList.addAll(characterList.filter {
                it.name.contains(
                    (txtSearchCharacter.value.toString()),
                    true
                )
            })
        }
    }

    fun onFilterClicked() {
        publish(EventShowSeasonDropdown())
    }

    fun loadCharacterList() {
        viewModelScope.launch {
            characterList.addAll(repository.getCharacters())

            filteredCharacterList.addAll(characterList)
            isListLoaded.value = true
        }
    }

    override fun onCleared() {
        super.onCleared()

        txtSearchCharacter.removeObserver { }
    }

    fun onSeasonSelected(title: String) {
        filteredCharacterList.clear()

        if (!txtSearchCharacter.value.isNullOrEmpty()) {
            txtSearchCharacter.value = ""
        }

        when (title) {
            "All" -> {
                filteredCharacterList.addAll(characterList)
            }
            "1" -> {
                filteredCharacterList.addAll(characterList.filter { it.appearance.contains(1) })
            }
            "2" -> {
                filteredCharacterList.addAll(characterList.filter { it.appearance.contains(2) })
            }
            "3" -> {
                filteredCharacterList.addAll(characterList.filter { it.appearance.contains(3) })
            }
            "4" -> {
                filteredCharacterList.addAll(characterList.filter { it.appearance.contains(4) })
            }
            "5" -> {
                filteredCharacterList.addAll(characterList.filter { it.appearance.contains(5) })
            }
        }
    }
}