package com.example.breakingbadapp.ui.fragment.details

import com.example.breakingbadapp.R
import com.example.breakingbadapp.databinding.FragmentDetailsBinding
import com.example.breakingbadapp.ui.fragment.BaseFragment

class DetailsFragment : BaseFragment<FragmentDetailsBinding>() {

    companion object {
        fun newInstance() = DetailsFragment()
    }

    override val layoutId: Int = R.layout.fragment_details
}
