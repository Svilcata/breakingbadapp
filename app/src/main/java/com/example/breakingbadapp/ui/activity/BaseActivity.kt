package com.example.breakingbadapp.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.breakingbadapp.R
import com.example.breakingbadapp.ui.fragment.BaseFragment

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun addFragment(fragment: BaseFragment<*>) {
        val ft = supportFragmentManager.beginTransaction()
        ft.add(R.id.content, fragment, fragment.tag)
        ft.commit()
    }

    fun replaceFragment(fragment: BaseFragment<*>) {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.content, fragment, fragment.tag)
        ft.addToBackStack(null)
        ft.commit()
    }
}